package com.fabric.common.enums;

/**
 * 操作状态
 * 
 * @author fabric
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
